(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
(** Command line executable file. *)

(** This file will generate an executable file. Depending on the
    options given in the command line, it will perform one of these
    operations: *)


(** [build_auto s f b] will parse [s] as an expression, build the
    corresponding labelled Petri net and draw it using {{:
    http://www.graphviz.org/} Graphviz} in the files: 
    - [<f>.gv] for the DOT representation; 
    - [<f>.png] for the image.

    If the boolean [b] is true, this function will also build the
    non-deterministic finite automaton of runs in the net, and draw it
    in similar manner in the files [<f>auto.gv] and [<f>auto.png].  *)
val build_auto : string -> string -> bool -> unit

(** [compare_expr eq fdest drawauto drawnet] will parse [eq] as an
    (in)equation, and test its validity. If the boolean [drawnet]
    holds, then the nets for both expressions will be drawn using the
    files [<fdest><j>.gv] and [<fdest><j>.png] (with [j] ranging over
    [1,2]). If the boolean [drawauto] holds, then the finite state
    automata computed will also be drawn, using files:
    - [<fdest>auto<j>.gv] and [<fdest>auto<j>.png] (with [j] ranging
    over [1,2]) if a single inequation has to be tested (comparisons
    [<=] or [=>]. 
    - [<fdest>_eq<k>_auto<j>.gv] and [<fdest>_eq<k>_auto<j>.png] (with
    [j] and [k] ranging over [1,2]) if two inequations have to be
    tested (comparisons [<>] [<] [>] [=/=] [=]).

*)
val compare_expr : string -> string -> bool -> bool -> unit

(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)

let build_auto e fdest b =
  let cmd f = Printf.sprintf "dot \"%s.gv\" -Tpng > \"%s.png\"" f f
  in
  let e =
    try Input.get_exp e
    with Parsing.Parse_error -> failwith ("Parsing problem: "^e)
  in
  let n = Net.net_of_expr e
  in
  Printf.printf "%s\n" (Output.string_of_expr e);
  Output.dot_of_net n (fdest^".gv");
  Printf.printf "%s\n" (cmd fdest);
  let _ = Unix.system (cmd fdest) in
  if b
  then
    let a : string Automaton.neauto =
      Net.auto_runs n
      |> Automaton.to_neauto
      |> Automaton.neauto_map
	   Tools.string_of_iset
    in
    Output.dot_of_neauto a (fdest^"auto.gv");
    Printf.printf "%s\n" (cmd (fdest^"auto"));
    let _ = Unix.system (cmd (fdest^"auto")) in
    ()
    
let compare_expr eq fdest drawauto drawnet = 
  let cmd f = Printf.sprintf "dot \"%s.gv\" -Tpng > \"%s.png\"" f f
  in
  let c,e,f =
    try Input.get_eq eq
    with Parsing.Parse_error -> failwith ("Parsing problem: "^eq)
  in
  let cmp e f n1 n2 fdest =
    Printf.printf "building runs automaton for %s..."
      (Output.string_of_expr e);
    print_newline ();
    let a : string Automaton.neauto =
      Net.auto_runs n1
      |> Automaton.to_neauto
      |> Automaton.neauto_map
	   Tools.string_of_iset
    in
    print_string "done.";
    print_newline ();
    if drawauto
    then
      begin
        Output.dot_of_neauto a (fdest^"auto1.gv");
        Printf.printf "running %s..." (cmd (fdest^"auto1"));
        print_newline ();
        let _ = Unix.system (cmd (fdest^"auto1")) in
        print_string "done.";
        print_newline ()
      end;
    print_string "building product automaton...";
    print_newline ();
    let b : string Automaton.neauto =
      Net.prod_auto n2 n1
      |> Automaton.to_neauto
      |> Automaton.neauto_map
	   Net.string_of_sim_state
    in
    print_string "done.";
    print_newline ();
    if drawauto
    then
      begin
        Output.dot_of_neauto b (fdest^"auto2.gv");
        Printf.printf "running %s..." (cmd (fdest^"auto2"));
        print_newline ();
        let _ = Unix.system (cmd (fdest^"auto2")) in
        print_string "done.";
        print_newline ()
      end;
    match Automaton.equal (Automaton.neauto_to_nauto a)
	    (Automaton.neauto_to_nauto b)
    with
    | None -> (Printf.printf "%s <= %s holds\n"
		 (Output.string_of_expr e)
		 (Output.string_of_expr f);
	       true)
    | Some w ->
       let tr = Net.get_trans n1 in
       let trlst = List.map (Net.trans_of_string tr) w in
       (* Printf.printf "%s\n" *)
       (* 		   (trlst *)
       (* 		    |> List.map Word.ptrans_of_trans *)
       (* 		    |> List.map Tools.string_of_ptrans *)
       (* 		    |> String.concat ";"); *)
       let cntrex = Expr.clean (Word.get_expr (Word.graph trlst)) in
       (Printf.printf "%s <= %s doesn't hold: %s\n"
	  (Output.string_of_expr e)
	  (Output.string_of_expr f)
	  (Output.string_of_expr cntrex);
        false)
  in
  Printf.printf "building net for %s..." (Output.string_of_expr e);
  print_newline ();
  let n1 = Net.net_of_expr e in
  print_string "done.";
  print_newline ();
  if drawnet
  then
    begin
      Output.dot_of_net n1 (fdest^"1.gv");
      Printf.printf "running %s..." (cmd (fdest^"1"));
      print_newline ();
      let _ = Unix.system (cmd  (fdest^"1")) in
      print_string "done.";
      print_newline ()
    end;
  Printf.printf "building net for %s..." (Output.string_of_expr e);
  print_newline ();
  let n2 = Net.net_of_expr f in
  print_string "done.";
  print_newline ();
  if drawnet
  then
    begin
      Output.dot_of_net n2 (fdest^"2.gv");
      Printf.printf "running %s..." (cmd (fdest^"2"));
      print_newline ();
      let _ = Unix.system (cmd  (fdest^"2")) in
      print_string "done.";
      print_newline ()
    end;
  let r = 
    match c with
    | Expr.Geq -> cmp f e n2 n1 fdest
    | Expr.Gt ->
       cmp f e n2 n1 (fdest^"_eq1_")
       && not (cmp e f n1 n2 (fdest^"_eq2_"))
    | Expr.Leq -> cmp e f n1 n2 fdest
    | Expr.Lt ->
       cmp e f n1 n2 (fdest^"_eq1_")
       && not (cmp f e n2 n1 (fdest^"_eq2_"))
    | Expr.Incomp ->
       not (cmp e f n1 n2 (fdest^"_eq1_")
	    || cmp f e n2 n1 (fdest^"_eq2_"))
    | Expr.Neq ->
       not (cmp e f n1 n2 (fdest^"_eq1_")
	    && cmp f e n2 n1 (fdest^"_eq2_"))
    | Expr.Eq ->
       (cmp e f n1 n2 (fdest^"_eq1_"))
       && (cmp f e n2 n1 (fdest^"_eq2_"))
  in if r
     then Printf.printf "%s holds.\n" eq
     else Printf.printf "%s doesn't hold.\n" eq
   
let quick_check eq verbose = 
  let c,e,f =
    try Input.get_eq eq
    with Parsing.Parse_error -> failwith ("Parsing problem: "^eq)
  in
  let cmp e f n1 n2 =
    match Net.test_incl n1 n2 with
    | None ->
       (if verbose then
          Printf.printf "%s <= %s holds\n"
	    (Output.string_of_expr e)
	    (Output.string_of_expr f);
	true)
    | Some w ->
       ((if verbose then
          let cntrex = Expr.clean (Word.get_expr (Word.graph (List.rev w))) in
          Printf.printf "%s <= %s doesn't hold: %s\n"
	    (Output.string_of_expr e)
	    (Output.string_of_expr f)
	    (Output.string_of_expr cntrex));
          false)
  in
  if verbose then (Printf.printf "building net for %s..." (Output.string_of_expr e);print_newline());
  let n1 = Net.net_of_expr e in
  if verbose then (print_string "done.";print_newline());
  if verbose then (Printf.printf "building net for %s..." (Output.string_of_expr f);print_newline());
  let n2 = Net.net_of_expr f in
  if verbose then (print_string "done.\n";print_newline());
  let r = 
    match c with
    | Expr.Geq -> cmp f e n2 n1
    | Expr.Gt ->
       cmp f e n2 n1
       && not (cmp e f n1 n2)
    | Expr.Leq -> cmp e f n1 n2
    | Expr.Lt ->
       cmp e f n1 n2
       && not (cmp f e n2 n1)
    | Expr.Incomp ->
       not (cmp e f n1 n2
	    || cmp f e n2 n1)
    | Expr.Neq ->
       not (cmp e f n1 n2
	    && cmp f e n2 n1)
    | Expr.Eq ->
       (cmp e f n1 n2)
       && (cmp f e n2 n1)
  in if r
     then Printf.printf "%s holds.\n" eq
     else Printf.printf "%s doesn't hold.\n" eq
   
  
let _ =
  let f = ref "net"
  and e = ref ""
  and dauto = ref false
  and dnet = ref false
  and v = ref false
  and b = ref false in
  let args_single =
    ["-o",Arg.Set_string f,
     "Set the destination name";
     "-u",Arg.Set b,
     "Unfold (only in single mode)"
    ]
  and args_compare =
    ["-o",Arg.Set_string f,
     "Set the destination name";
     "--draw-auto",Arg.Set dauto,
     "Draw automata";
     "--draw-net",Arg.Set dnet,
     "Draw nets";
     "--draw",Arg.Unit (fun () -> dauto:=true;dnet:=true),
     "Draw automata and nets"
    ]
  and args_quick =
    ["-v",Arg.Set v,"Verbose mode"]
  in
  let args = ref []
  and todo = ref (fun _ -> ())
  in
  let set_cmd str =
    if !Arg.current = 1
    then
      begin
        match str with
        | "compare" -> (args := args_compare;todo:=(fun () -> compare_expr !e !f !dauto !dnet))
        | "single" -> (args:= args_single;todo:=(fun () -> build_auto !e !f !b))
        | "quick-check" -> (args:=args_quick;todo:=(fun () -> quick_check !e !v))
        | _ -> failwith ("Unknown command: "^str)
      end
    else e:=str
  in
  Arg.parse_dynamic args set_cmd "Use : <execname> <cmd> [OPTIONS] <eq>";
  !todo()

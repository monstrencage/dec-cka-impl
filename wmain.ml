(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)

module Html = Dom_html

exception NotDefined

let handle f x = 
  try f x
  with NotDefined -> ()


let initex1 = "(a|b).(c|d)"

let initex2 = "(a.c|b.d)"

let initeq = "(a|b).(c|d) < (a.c|b.d)"

let replace_child p n =
  Js.Opt.iter (p##firstChild) (fun c -> Dom.removeChild p c);
  Dom.appendChild p n

let textbox id (action : string -> unit) default =
  let elt = 
    Js.Opt.get
      Html.document##getElementById(Js.string id)
      (fun () -> raise NotDefined)
  in
  let t = Html.createInput Html.document in
  replace_child elt t;
  t##size <- 20;
  t##className <- Js.string "drawin";
  t##onchange <- Html.handler
                   (fun e ->
                     let text = Js.to_string (t##value) in
                     action text;
                     Js._false);
  t##value <- Js.string default

let print_net_app id_in =
  let aux id_out =
    let e = ref initex1 in
    let upd () =
      try
        let e = Input.get_exp (!e) in
        let n = Net.net_of_expr e in
        Woutput.vis_of_net n id_out
      with Parsing.Parse_error -> ()
    in
    textbox id_in (fun s -> e:=s; upd ()) (!e);
    upd()
  in handle aux

let compare_expr_app id_in1 id_in2 id_res
                     id_net1 id_net2 id_auto1 =
  let aux id_auto2 =
    let r =
      Js.Opt.get
        Html.document##getElementById(Js.string id_res)
        (fun () -> raise NotDefined)
    in
    let e1,e2 = ref initex1,ref initex2 in
    let upd () =
      try
        let txt1,txt2 = !e1,!e2 in
        let e = Input.get_exp txt1
        and f = Input.get_exp txt2 in
        let n1 = Net.net_of_expr e
        and n2 = Net.net_of_expr f in
        Woutput.vis_of_net n1 id_net1;
        Woutput.vis_of_net n2 id_net2;
        let a1 = 
          Net.auto_runs n1
          |> Automaton.to_neauto
          |> Automaton.neauto_map
	       Tools.string_of_iset
        in
        Woutput.vis_of_neauto a1 id_auto1;
        let a2 = 
          Net.prod_auto n2 n1
          |> Automaton.to_neauto
          |> Automaton.neauto_map
               Net.string_of_sim_state
        in
        Woutput.vis_of_neauto a2 id_auto2;
        match Automaton.equal (Automaton.neauto_to_nauto a1)
			      (Automaton.neauto_to_nauto a2)
        with
        | None ->
           r##innerHTML <-
             Js.string
               (Printf.sprintf "%s <= %s holds\n"
		               (Output.string_of_expr e)
		               (Output.string_of_expr f))
        | Some w ->
           let tr = Net.get_trans n1 in
           let trlst = List.map (Net.trans_of_string tr) w in
           let cntrex = Expr.clean (Word.get_expr (Word.graph trlst)) in
           r##innerHTML <-
             Js.string
               (Printf.sprintf "%s <= %s doesn't hold: %s\n"
		               (Output.string_of_expr e)
		               (Output.string_of_expr f)
		               (Output.string_of_expr cntrex))
      with Parsing.Parse_error -> ()
    in
    textbox id_in1 (fun s -> e1:= s;upd ()) (!e1); 
    textbox id_in2 (fun s -> e2:= s;upd ()) (!e2);
    upd()
  in handle aux


let cmp (c,e,f as eq) res img =
  let msg = ref "" in
  let cmp e f n1 n2 =
    let a : string Automaton.neauto =
      Net.auto_runs n1
      |> Automaton.to_neauto
      |> Automaton.neauto_map
	   Tools.string_of_iset
    in
    let b : string Automaton.neauto =
      Net.prod_auto n2 n1
      |> Automaton.to_neauto
      |> Automaton.neauto_map
	   Net.string_of_sim_state
    in
    match Automaton.equal (Automaton.neauto_to_nauto a)
			  (Automaton.neauto_to_nauto b)
    with
    | None ->
       (msg := Printf.sprintf "%s%s <= %s holds<br>\n"
                              (!msg)
			      (Output.string_of_expr e)
			      (Output.string_of_expr f);
	true)
    | Some w ->
       let tr = Net.get_trans n1 in
       let trlst = List.map (Net.trans_of_string tr) w in
       let cntrex = Expr.clean (Word.get_expr (Word.graph trlst)) in
       (msg := Printf.sprintf "%s%s <= %s doesn't hold: %s<br>\n"
                              (!msg)
		              (Output.string_of_expr e)
		              (Output.string_of_expr f)
		              (Output.string_of_expr cntrex);
        false)
  in
  let n1 = Net.net_of_expr e in
  let n2 = Net.net_of_expr f in
  let r = 
    match c with
    | Expr.Geq -> cmp f e n2 n1
    | Expr.Gt ->
       cmp f e n2 n1
       && not (cmp e f n1 n2)
    | Expr.Leq -> cmp e f n1 n2
    | Expr.Lt ->
       cmp e f n1 n2
       && not (cmp f e n2 n1)
    | Expr.Incomp ->
       not (cmp e f n1 n2
	    || cmp f e n2 n1)
    | Expr.Neq ->
       not (cmp e f n1 n2
	    && cmp f e n2 n1)
    | Expr.Eq ->
       (cmp e f n1 n2)
       && (cmp f e n2 n1)
  in
  if r
  then
    (res##innerHTML<-
       (Js.string
          (Printf.sprintf "%s%s holds."
                          (!msg)
                          (Output.string_of_eq eq)));
     img##width <- 20;
     img##src <- (Js.Unsafe.variable "window")##ok)
  else
    (res##innerHTML<-
       (Js.string
          (Printf.sprintf "%s%s doesn't hold."
                          (!msg)
                          (Output.string_of_eq eq)));
     img##width <- 15;
     img##src <- (Js.Unsafe.variable "window")##nope)

let solve_app id_in id_out =
  let aux id_img =
    let e = ref initeq in
    let res = 
      Js.Opt.get
        Html.document##getElementById(Js.string id_out)
        (fun () -> raise NotDefined)
    in
    let img =
      let i =
        Js.Opt.get
          Html.document##getElementById(Js.string id_img)
          (fun () -> raise NotDefined)
      in
      let i1 = Html.createImg Html.document in
      Dom.appendChild i i1;
      i1
    in
    let upd () =
      try
        let eq = Input.get_eq (!e) in
        cmp eq res img
      with _ -> ()
    in
    textbox id_in (fun s -> e:= s;upd ()) (!e); 
    upd ()
  in handle aux
  
let onload _ = 
  print_net_app "draw_in" "draw_out";
  compare_expr_app "solve_in1" "solve_in2" "solve_res"
                   "solve_net1" "solve_net2" "solve_auto1"
                   "solve_auto2";
  solve_app "solve_in" "solve_out" "solve_img";
  Js._false

 
let _ =
  Html.window##onload <- Html.handler onload


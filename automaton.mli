(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
(** Finite state automata. *)

(** Type of transitions with {%html: &epsilon;%} between states of type 'a. *)
type 'a etrans = 'a * string option * 'a

(** Type of transitions without {%html: &epsilon;%} between states of type 'a. *)
type 'a trans = 'a * string * 'a

(** Non-deterministic automata with {%html: &epsilon;%}-transitions. *)
type 'a neauto = 'a list * 'a etrans list * 'a list * 'a list

(** Non-deterministic automata without transitions. *)
type 'a nauto = 'a list * 'a trans list * 'a list * 'a list

(** Deterministic automata. *)
type 'a dauto = 'a list * Tools.SSet.t * ('a -> string -> 'a) * 'a * 'a list

(** Finite state automata. *)
type 'a auto = NE of 'a neauto | N of 'a nauto | D of 'a dauto

(** [neauto_map f a] relabels the automaton [a] using [f], assuming [f] is an injection. *)
val neauto_map : ('a -> 'b) -> 'a neauto -> 'b neauto

(** Converts any automaton to a non-deterministic automaton with {%html: &epsilon;%}-transitions. *)
val to_neauto : 'a auto -> 'a neauto

(** Removes the epsilon transitions from an automaton that uses strings as states. *)
val neauto_to_nauto : string neauto -> string nauto

(** Tests language equivalence of non-deterministic automata without
    {%html: &epsilon;%}-transitions over [string] states. *)
val equal : string nauto -> string nauto -> Tools.SMap.key list option

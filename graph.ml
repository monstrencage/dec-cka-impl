(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
   02110-1301 USA.*)
open Tools

type io_hypergraph = string IMap.t * string IMap.t * (int * int) list * ISet.t

let dot_of_graph (m,t,a,i : io_hypergraph) file =
  let printarc ch (i,j) = Printf.fprintf ch "%d -> %d;\n" i j in
  let printtrans ch k s = 
    Printf.fprintf ch "%d [label=\"%s\";shape=rectangle];\n" k s
  in
  let printplace ch i s = 
    Printf.fprintf ch "%d [label=\"%s\";shape=circle];\n" i s
  in
  let printpoint ch i = 
    Printf.fprintf ch "%d [shape=point];\n" i
  in
  let ch = open_out file in
  Printf.fprintf ch "digraph structs {\n";
  ISet.iter (printpoint ch) i;
  IMap.iter (printplace ch) m;
  IMap.iter (printtrans ch) t;
  List.iter (printarc ch) a;
  Printf.fprintf ch " }";
  close_out ch


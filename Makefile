# Copyright (C) 2016 Paul Brunet

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

EXEC=cka
WEB=$(EXEC).js
MAIN=main
WMAIN=wmain

WDIR=web/javascripts/
SRC=automaton.ml automaton.mli expr.mli input.ml input.mli main.ml   \
	main.mli net.ml net.mli parser.mly wmain.ml wmain.mli	     \
	word.mli expr.ml graph.ml graph.mli lexer.mll output.ml	     \
	output.mli tools.ml tools.mli word.ml woutput.ml woutput.mli
DOCDIR=doc.docdir
WDOCDIR=web/Files/cka.doc
DOC=$(DOCDIR)/index.html

WFLAGS= -use-ocamlfind -pkgs js_of_ocaml,js_of_ocaml.syntax -syntax camlp4o
FLAGS= -lib unix
OCAML= ocamlbuild

all :: byte opt web doc
opt : $(EXEC).opt
byte : $(EXEC)
web : $(WEB)
doc : $(DOC)
clean :
	rm -rf *~ *.gv *.png $(EXEC) $(EXEC).opt _build \
	.depend* $(WDIR)$(WEB) $(WMAIN).byte $(DOCDIR)

$(EXEC) :
	$(OCAML) $(FLAGS) $(MAIN).byte
	mv  $(MAIN).byte $(EXEC)

$(EXEC).opt :
	$(OCAML) $(FLAGS) $(MAIN).native
	mv  $(MAIN).native $(EXEC).opt

$(WMAIN).byte : $(SRC)
	$(OCAML) $(FLAGS) $(WFLAGS) $(WMAIN).byte

$(WEB) : $(WMAIN).byte $(WDIR)
	js_of_ocaml $(WMAIN).byte
	mv $(WMAIN).js $(WDIR)$(WEB)

$(WDIR) :
	mkdir -p $(WDIR)

$(WDOCDIR) : 
	mkdir -p $(WDOCDIR)

$(DOC) : $(WDOCDIR)
	$(OCAML) $(DOC)
	mv _build/$(DOCDIR)/* $(WDOCDIR)/

(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
open Tools

type 'a etrans = 'a * string option * 'a
type 'a trans = 'a * string * 'a
		       
type 'a neauto =
    'a list * 'a etrans list * 'a list * 'a list
type 'a nauto = 
    'a list * 'a trans list * 'a list * 'a list
type 'a dauto =
    'a list * SSet.t * ('a -> string -> 'a) * 'a * 'a list

type 'a auto = NE of 'a neauto | N of 'a nauto | D of 'a dauto

let neauto_map (f : 'a -> 'b) : 'a neauto -> 'b neauto = function
  | (s,t,i,o) ->
     (List.map f s,
      List.map (fun (p,a,q) -> f p,a,f q) t,
      List.map f i, List.map f o)

let to_neauto : 'a auto -> 'a neauto = function
  | NE a -> a
  | N (q,t,i,f) -> (q,List.map (fun (p,a,q) -> p,Some a,q) t,i,f)
  | D (q,a,d,i,f) ->
     (q,
      (List.fold_left
	 (fun acc p ->
	  SSet.fold
	    (fun a acc ->
	     (p,Some a,d p a)::acc)
	    a acc)
	 [] q),
      [i],
      f)

let prod (r1 : SPSet.t) (r2 : SPSet.t) =
  SPSet.fold
    (fun (p,q) ->
     SPSet.fold
       (fun (r,s) acc ->
	if q = r
	then SPSet.add (p,s) acc
	else acc)
       r2)
    r1 SPSet.empty

let id s =
  List.fold_left (fun acc p -> SPSet.add (p,p) acc) SPSet.empty s

let refl_trans_clos s r =
  let rec aux acc =
    let acc' = prod acc acc in
    if SPSet.equal acc acc'
    then acc
    else aux acc'
  in aux (SPSet.union r (id s))

let trans_clos s r =
  refl_trans_clos s r
  |> SPSet.filter (fun (p,q) -> p<> q)

let pred p r =
  SPSet.fold (fun (q,r) acc -> if r=p then SSet.add q acc else acc)
	     r SSet.empty
let succ p r =
  SPSet.fold (fun (q,r) acc -> if q=p then SSet.add r acc else acc)
	     r SSet.empty
	 
let neauto_to_nauto (q,t,i,f : string neauto) : string nauto=
  let (tr,eps) =
    List.fold_left
      (fun (tr,eps) ->
       function
       | (p,Some a,q) -> (p,a,q)::tr,eps
       | (p,None,q) -> tr,(SPSet.add (p,q) eps))
      ([],SPSet.empty)
      t
  in
  let eps' = refl_trans_clos q eps in
  let augm acc (p,a,q) =
    SSet.fold
      (fun p' acc -> (p',a,q)::acc)
      (pred p eps')
      acc
  in
  let f0 = List.fold_left (fun acc f -> SSet.add f acc) SSet.empty f in
  let fn =
    List.filter
      (fun p ->
       not (SSet.is_empty
	      (SSet.inter f0 (succ p eps'))))
      q
  in
  (q,List.fold_left augm [] tr,i,fn)

let next q t =
  List.fold_left
    (fun acc (p,a,p') ->
     if SSet.mem p q
     then SMap.add a
		   (SSet.add p'
			     (try SMap.find a acc
			      with Not_found -> SSet.empty))
		   acc
     else acc)
    SMap.empty
    t

let equal (q1,t1,i1,f1 : string nauto) (q2,t2,i2,f2: string nauto) =
  let fn1 = List.fold_left (fun acc f -> SSet.add f acc) SSet.empty f1
  and fn2 = List.fold_left (fun acc f -> SSet.add f acc) SSet.empty f2
  and i1 = List.fold_left (fun acc f -> SSet.add f acc) SSet.empty i1
  and i2 = List.fold_left (fun acc f -> SSet.add f acc) SSet.empty i2
  in
  let wrong p q =
    match SSet.is_empty (SSet.inter p fn1),
          SSet.is_empty (SSet.inter q fn2)
    with false,true | true,false -> true
         | _ -> false
  in
  let rec aux sim = function
    | [] -> None
    | (w,p,q)::todo ->
       if wrong p q
       then Some (List.rev w)
       else
	 let n =
           SMap.merge
             (fun _ a b ->
               match a,b with
               | None,None -> None
               | Some a,None -> Some (a,SSet.empty)
               | None,Some b -> Some (SSet.empty,b)
               | Some a,Some b -> Some (a,b))
             (next p t1) (next q t2)
         in
	 let sim,todo =
	   SMap.fold
	     (fun a (p1,q1) (sim,td) ->
	       if SSCaseUF.equivalent
                    (Left p1) (Right q1) sim
	       then (sim,td)
	       else (SSCaseUF.union (Left p1) (Right q1) sim,
                     (a::w,p1,q1)::td))
	     n
	     (sim,todo)
	 in aux sim todo
  in
  if wrong i1 i2
  then Some []
  else aux (SSCaseUF.union (Left i1) (Right i2) SSCaseUF.initial)
           [[],i1,i2]

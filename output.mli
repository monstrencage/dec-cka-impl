(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
(** Output functions. *)

(** String representation of expressions. *)
val string_of_expr : Expr.t -> string

(** String representation of comparisons. *)
val string_of_comp : Expr.comp -> string

(** String representation of equations. *)
val string_of_eq : Expr.eq -> string

(** Translates a net as a graph. *)
val graph_of_net : Net.net -> Graph.io_hypergraph

(** [dot_of_net n f] writes a description of [n] in the file [f],
    using the language [DOT] of {{: http://www.graphviz.org/ }
    Graphviz }. *)
val dot_of_net : Net.net -> string -> unit

(** Translates a non-deterministic automaton with {%html:
    &epsilon;%}-transition over string states as a graph. *)
val graph_of_neauto : string Automaton.neauto -> Graph.io_hypergraph

(** [dot_of_neauto a f] writes a description of [a] in the file [f],
    using the language [DOT] of {{: http://www.graphviz.org/ }
    Graphviz }. *)
val dot_of_neauto : string Automaton.neauto -> string -> unit

(** [dot_of_auto a f] writes a description of [a] in the file [f],
    using the language [DOT] of {{: http://www.graphviz.org/ }
    Graphviz }. *)
val dot_of_auto : string Automaton.auto -> string -> unit

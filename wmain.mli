(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
(** Javascript executable file. *)

(** If executed as a [Js] file, this module will display the following
    applications on the current webpage. *)


(** [print_net_app idin idout] creates an app that creates a textbox
    inside the html element of id [idin], and dynamically reads its
    content as an expression, drawing the corresponding net inside the
    html element of id [idout]. If either [idin] or [idout] are not
    present on the current page, nothing happens. *)
val print_net_app : string -> string -> unit

(** [compare_expr_app idin1 idin2 idres idnet1 idnet2 idauto1 idauto2]
    creates an app that creates two textboxes inside the html elements
    of id [idin1] and [idin2], and dynamically reads their content as
    expressions. The corresponding nets are drawn inside the html
    elements of id [idnet1] and [idnet2]. Then the automaton for the
    runs of the first net is computed and diplayed in [idauto1]. The
    product automaton of the two nets is computed and displayed in
    [idauto2]. These two finite state automata are compared, and from
    this the containement of the first expression inside the second is
    inferred, the result being printed in the html element of id
    [idres]. If any of the elements required is missing, nothing
    happens. *)
val compare_expr_app :
  string -> string -> string -> string -> string -> string -> string -> unit

(** [solve_app idin idout idpic] creates an app that creates a textbox
    inside the html element of id [idin], dynamically reads its
    content as an equation, tests its validity, and prints its
    conclusions inside the html element of id [idout]. Futhermore, it
    uses the element [idpic] to diplay a picture corresponding to the
    validity of the equation:
    - if the equation holds the picture referenced in the [Js]
      variable [ok] is used;
    - otherwise the one described in the variable [nope] is used.

    If one of the required ids is not present nothing happens.  *)
val solve_app : string -> string -> string -> unit

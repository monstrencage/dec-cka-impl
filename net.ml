(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)

open Tools
open Expr

type ptrans = int * Tools.ISet.t * string option * Tools.ISet.t
module Trans = struct
  type t = ptrans
  let compare (k,s,a,t) (k',s',a',t') =
    match Pervasives.compare (k,a) (k',a') with
    | 0 ->
       begin
	 match ISet.compare s s' with
	 | 0 -> ISet.compare t t'
	 | k -> k
       end
    | k -> k
end
module TSet = Set.Make(Trans)
		      
type net = ISet.t * int * int * TSet.t

let rec net_of_expr : Expr.t -> net =
  let rec aux k tk = function
    | Zero ->
       (ISet.of_list [k;k+1],k,k+1,TSet.empty),(k+2),tk
    | Un ->
       (ISet.of_list [k],k,k,TSet.empty),(k+1),tk
    | V a ->
       (ISet.of_list [k;k+1],
	k,
	k+1,
	TSet.singleton
	  (tk,ISet.singleton k,Some a,ISet.singleton (k+1))),
       (k+2),
       (tk+1)
    | Union (e,f) ->
       let ((s1,i1,o1,t1),k1,tk1) = aux k tk e in
       let ((s2,i2,o2,t2),k2,tk2) = aux k1 tk1 f in
       let i3 = k2 and o3 = k2+1 in
       let s3 = ISet.union s1 s2 |> ISet.union (ISet.of_list [i3;o3])
       and t3 =
	 TSet.union t1 t2
	 |> TSet.union
	      (TSet.of_list
		 [tk2,ISet.singleton i3,None,ISet.singleton i1;
		  tk2+1,ISet.singleton i3,None,ISet.singleton i2;
		  tk2+2,ISet.singleton o1,None,ISet.singleton o3;
		  tk2+3,ISet.singleton o2,None,ISet.singleton o3])
       in (s3,i3,o3,t3),(o3+1),(tk2+4)		       
    | Seq (e,f) ->
       let ((s1,i1,o1,t1),k1,tk1) = aux k tk e in
       let ((s2,i2,o2,t2),k2,tk2) = aux k1 tk1 f in
       let s3 = ISet.union s1 s2
       and t3 =
	 TSet.union t1 t2
	 |> TSet.union
	      (TSet.singleton
		 (tk2,ISet.singleton o1,None,ISet.singleton i2))
       in (s3,i1,o2,t3),k2,(tk2+1)
    | Par (e,f) ->
       let ((s1,i1,o1,t1),k1,tk1) = aux k tk e in
       let ((s2,i2,o2,t2),k2,tk2) = aux k1 tk1 f in
       let i3 = k2 and o3 = k2+1 in
       let s3 = ISet.union s1 s2 |> ISet.union (ISet.of_list [i3;o3])
       and t3 =
	 TSet.union t1 t2
	 |> TSet.union
	      (TSet.of_list
		 [tk2,ISet.singleton i3,None,ISet.of_list [i1;i2];
		  tk2+1,ISet.of_list [o1;o2],None,ISet.singleton o3])
       in (s3,i3,o3,t3),(o3+1),(tk2+2)
    | Star e  ->
       let ((s1,i1,o1,t1),k1,tk1) = aux k tk e in
       let i3 = k1 and o3 = k1+1 in
       let s3 = ISet.union s1 (ISet.of_list [i3;o3])
       and t3 = TSet.union
		  t1
		  (TSet.of_list
		     [tk1,ISet.singleton i3,None,ISet.singleton i1;
		      tk1+1,ISet.singleton o1,None,ISet.singleton i1;
		      tk1+2,ISet.singleton o1,None,ISet.singleton o3;
		      tk1+3,ISet.singleton i3,None,ISet.singleton o3])
       in (s3,i3,o3,t3),(o3+1),(tk1+4)
  in fun e -> let n,_,_ = (aux 0 0 e) in n

open Automaton

let ready s (_,q,_,_) : bool= ISet.subset q s

let next s (_,p,_,q) : ISet.t =
 (ISet.diff s p) |> ISet.union q

let lbl (k,_,a,_) =
  match a with
  | None -> Printf.sprintf "%d:τ" k
  | Some a -> Printf.sprintf "%d:%s" k a
			     
let auto_runs (s,i,o,t) : ISet.t auto =
  let q0 = ISet.singleton i
  and qf = ISet.singleton o in
  let rec aux states trans = function
    | [] -> states,trans
    | p::todo ->
       let st,tr,td =
	 TSet.fold (fun t (st,tr,td) ->
		    if ready p t
		    then
		      let q = next p t
		      and a = lbl t in
		      if ISSet.mem q st
		      then (st,(p,a,q)::tr,td)
		      else (ISSet.add q st,(p,a,q)::tr,q::td)
		    else (st,tr,td))
		   t (states,trans,todo)
       in aux st tr td
  in
  let st,tr = aux (ISSet.of_list[q0;qf]) [] [q0;qf] in
  N (ISSet.elements st,tr,[q0],[qf])



type sim_state =
    ISet.t * ISet.t * IPSet.t

let move_tau_1 (c,c',r : sim_state) (_,s,x,t : Trans.t)
  : sim_state etrans option =
  if x = None && ISet.subset s c
  then
    let d = ISet.union (ISet.diff c s) t in
    let pre,stay =
      IPSet.partition (fun (p,q) -> ISet.mem p s) r
    in
    let curry = IPSet.fold add_pair pre IMap.empty in
    let succ =
      try
	IMap.fold (fun _ s -> ISet.inter s)
		  curry
		  (IMap.choose curry |> snd)
      with Not_found -> ISet.empty
    in
    let r' = ISet.fold (fun p ->
			ISet.fold (fun q -> IPSet.add (p,q))
				  succ)
		       t stay
    in
    Some ((c,c',r),None,(d,c',r'))
  else None

let move_tau_2 (c,c',r : sim_state) (k,s,x,t : Trans.t)
  : sim_state etrans option =
  if x = None && ISet.subset s c'
  then
    let d' = ISet.union (ISet.diff c' s) t in
    let pre,stay =
      IPSet.partition (fun (p,q) -> ISet.mem q s) r
    in
    let curry = IPSet.fold (fun (p,_) -> ISet.add p) pre ISet.empty in
    let r' = ISet.fold (fun q ->
			ISet.fold (fun p -> IPSet.add (p,q))
				  curry)
		       t stay
    in
    Some ((c,c',r),Some (lbl (k,s,x,t)), (c,d',r'))
  else None

let move_var (c,c',r : sim_state)
	     (k,s,x,t : Trans.t)
	     (k',s',x',t' : Trans.t)
    : sim_state etrans option =
  match (x,x',ISet.subset s c,ISet.subset s' c') with
  | _,_,false,_
  | _,_,_,false
  | None,_,_,_
  | _,None,_,_ -> None
  | Some x,Some y,_,_ when x <> y -> None
  | _ ->
     let p0 = ISet.choose s
     and p1 = ISet.choose t
     and q0 = ISet.choose s'
     and q1 = ISet.choose t' in
     let d = ISet.union (ISet.diff c s) t 
     and d' = ISet.union (ISet.diff c' s') t' in
     let predq0,stay =
       let pr,st = IPSet.partition (fun (_,q) -> q=q0) r in
       IPSet.fold (fun (p,_) -> ISet.add p) pr ISet.empty,st 
     in
     if ISet.mem p0 predq0
     then
       let r' =
	 ISet.fold (fun p -> if p=p0
			     then IPSet.add (p1,q1)
			     else IPSet.add (p,q1))
		   predq0
		   (IPSet.filter (fun (p,_) -> p<> p0) stay)
       in
       Some ((c,c',r),Some (lbl (k',s',x',t')), (d,d',r'))
     else None

module SimStSet =
  Set.Make(struct
	    type t = sim_state
	    let compare (c1,d1,r1) (c2,d2,r2) =
	      match ISet.compare c1 c2 with
	      | 0 ->
		 begin
		   match ISet.compare d1 d2 with
		   | 0 -> IPSet.compare r1 r2
		   | k -> k
		 end
	      | k -> k
	  end)

let letter (_,_,a,_) = a

let string_of_sim_state (c,c',r : sim_state) =
  Printf.sprintf
    "%s\n%s\n%s"
    (string_of_iset c)
    (string_of_iset c')
    (string_of_ipset r)
     
	  
let prod_auto (p1,i1,o1,t1 : net) (p2,i2,o2,t2 : net)
    : sim_state auto =
  let q0 = ISet.singleton i1,
	   ISet.singleton i2,
	   IPSet.singleton (i1,i2)
  in
  let tt1,ta1 = TSet.partition
		  (function (_,_,None,_) -> true | _ -> false)
		  t1
  and tt2,ta2 = TSet.partition
		  (function (_,_,None,_) -> true | _ -> false)
		  t2
  in
  let ta =
    TSet.fold
      (fun t1 ->
       TSet.fold
	 (fun t2 acc ->
	  if (letter t1 = letter t2)
	  then (t1,t2)::acc
	  else acc)
	 ta2)
      ta1
      []
  in 
  let rec aux states trans = function
    | [] -> states,trans
    | p::todo ->
       let st,tr,td =
	 List.fold_left (fun (st,tr,td) (t1,t2) ->
			    match move_var p t1 t2 with
			    | None -> (st,tr,td)
			    | Some (_,_,q as t) ->
			       if SimStSet.mem q st
			       then (st,t::tr,td)
			       else (SimStSet.add q st,t::tr,q::td))
			(states,trans,todo)
			ta
	 |> TSet.fold (fun t1 (st,tr,td) ->
		       match move_tau_1 p t1 with
		       | None -> (st,tr,td)
		       | Some (_,_,q as t) ->
			  if SimStSet.mem q st
			  then (st,t::tr,td)
			  else (SimStSet.add q st,t::tr,q::td))
		      tt1 
	 |> TSet.fold (fun t2 (st,tr,td) ->
		       match move_tau_2 p t2 with
		       | None -> (st,tr,td)
		       | Some (_,_,q as t) ->
			  if SimStSet.mem q st
			  then (st,t::tr,td)
			  else (SimStSet.add q st,t::tr,q::td))
		      tt2
		   
	 
       in aux st tr td
  in
  let st,tr = aux (SimStSet.of_list[q0]) [] [q0] in
  let states = SimStSet.elements st in
  let fn = List.filter (fun (c,c',_) ->
			ISet.equal c (ISet.singleton o1)
			&& ISet.equal c' (ISet.singleton o2))
		       states
  in
  NE (states,tr,[q0],fn)

let trans_of_string tr s =
  let rec aux acc j =
    try 
      match s.[j] with
      | ':' -> acc
      | k ->
	 let d = int_of_char k - int_of_char '0' in
	 if 0<= d && d <= 9
	 then aux (10*acc + d) (j+1)
	 else failwith "trans_of_string: not a digit"
    with Invalid_argument _ ->
      failwith "trans_of_string: string too short"
  in
  let k0 = aux 0 0 in
  let candidates =
    TSet.filter (fun (k,_,_,_) -> k = k0) tr
  in
  if TSet.cardinal candidates = 1
  then TSet.choose candidates
  else failwith "trans_of_string: several transitions with the same id"

let get_trans (_,_,_,t : net) = t


type a_state = ISet.t
type b_state = ISet.t * IPSet.t

module CmpStateSet =
  Set.Make
    (struct
      type t = b_state
      let compare (x,y) (x',y') =
        match ISet.compare x x' with
        | 0 -> IPSet.compare y y'
        | k -> k
    end)

type det_b_state = CmpStateSet.t
  
module SimRelRange = Set.Make(CmpStateSet)
  
module SimMap = Map.Make(ISet)

type sim_rel = SimRelRange.t SimMap.t

type cmp_state = a_state * det_b_state
             
let init_sim : sim_rel = SimMap.empty

let targets : sim_rel -> a_state -> SimRelRange.t =
  fun r q ->
  try SimMap.find q r
  with Not_found -> SimRelRange.empty
    
let add_link : sim_rel -> a_state -> det_b_state -> sim_rel =
  fun r q qs -> SimMap.add q (SimRelRange.add qs (targets r q)) r
    
let test_link : sim_rel -> a_state -> det_b_state -> bool =
  fun r q qs -> SimRelRange.mem qs (targets r q)

let fire_tau_1 : cmp_state -> ptrans -> cmp_state option =
  fun (p,qs) (_,pre,lbl,post as t) ->
  match lbl with
  | Some _ -> None
  | None ->
     if ready p t
     then
       let p' = next p t in
       let qs' =
         CmpStateSet.fold
           (fun (q,r) acc ->
             let (r1,r2) = IPSet.partition (fun (x,y) -> ISet.mem x pre) r in
             let q1 = IPSet.fold (fun (x,y) acc -> ISet.add y acc) r1 ISet.empty in
             let r' = ISet.fold (fun pi acc -> ISet.fold (fun qi acc -> IPSet.add (pi,qi) acc) q1 acc)
                        post r2 in
             CmpStateSet.add (q,r') acc)
           qs
           CmpStateSet.empty
       in
       Some (p',qs')
     else None 

let fire_tau_b : b_state -> ptrans -> b_state option =
  fun (q,r) (_,pre,lbl,post as t) ->
  match lbl with
  | Some _ -> None
  | None ->
     if ready q t
     then
       let q' = next q t in
       let (r1,r2) = IPSet.partition (fun (x,y) -> ISet.mem y pre) r in
       let p' = IPSet.fold (fun (x,y) acc -> ISet.add x acc) r1 ISet.empty in
       let pp' = ISet.fold (fun q0 acc -> ISet.filter (fun p0 -> IPSet.mem (p0,q0) r1) acc) pre p' in
       let r' = ISet.fold (fun p0 acc -> ISet.fold (fun q0 acc -> IPSet.add (p0,q0) acc) post acc)
                  pp' r2 in
       Some (q',r')
     else None

let fire_tau_2 : cmp_state -> TSet.t -> cmp_state =
  fun (p,qs) tt ->
  let tt' = TSet.elements tt in
  let rec aux acc = function
    | [] -> acc
    | qs::td ->
       begin
         let acc',td' =
           List.fold_left
             (fun (acc,td) t ->
               match fire_tau_b qs t with
               | None -> (acc,td)
               | Some q' ->
                  if CmpStateSet.mem q' acc
                  then (acc,td)
                  else (CmpStateSet.add q' acc,q'::td))
             (acc,td)
             tt'
         in aux acc' td'
       end
  in (p,aux qs (CmpStateSet.elements qs))

let fire_var : cmp_state -> ptrans -> ptrans -> cmp_state option =
  fun (p,qs) (_,pre1,l1,post1 as t1) (_,pre2,l2,post2 as t2) ->
  match l1,l2 with
  | None,_ | _,None -> None
  | Some a,Some b ->
     if a = b && ready p t1
     then
       let p' = next p t1 
       and p0 = ISet.choose pre1
       and p1 = ISet.choose post1
       and q0 = ISet.choose pre2
       and q1 = ISet.choose post2 in
       let qs' =
         CmpStateSet.fold
           (fun (q,r) acc ->
             if IPSet.mem (p0,q0) r
             then
               let q' = next q t2 in
               let r1,r2 = IPSet.partition (fun (x,y) -> x = p1 || x = p0 || y = q0 || y = q1) r in
               let r' = IPSet.fold
                           (fun (x,y) acc ->
                             if x = p0 && ISet.mem y q'
                             then IPSet.add (p1,y) acc
                             else acc)
                           r1
                           (IPSet.add (p1,q1) r2)
               in
               CmpStateSet.add (q',r') acc
             else acc
           )
           qs
           CmpStateSet.empty
       in
       Some (p',qs')
     else None
  
   
let test_incl (p1,i1,o1,t1 : net) (p2,i2,o2,t2 : net) : ptrans list option =
  let tt1,ta1 = TSet.partition
		  (function (_,_,None,_) -> true | _ -> false)
		  t1
  and tt2,ta2 = TSet.partition
		  (function (_,_,None,_) -> true | _ -> false)
		  t2
  in
  let ta =
    TSet.fold
      (fun t1 ->
       TSet.fold
	 (fun t2 acc ->
	  if (letter t1 = letter t2)
	  then (t1,t2)::acc
	  else acc)
	 ta2)
      ta1
      []
  in
  let final1 q = ISet.equal (fst q) (ISet.singleton o1) in
  let final2 q = CmpStateSet.mem (ISet.singleton o2,IPSet.singleton (o1,o2)) (snd q) in
  let rec check (r :sim_rel) : (ptrans list * cmp_state) list -> ptrans list option =
    function
    | [] -> None
    | (h,q)::todo ->
       begin
         (* Printf.printf "trying %s <= %s\n"
        *     (string_of_iset (fst q))
        *     (CmpStateSet.fold (fun (q0,r0) acc ->
        *          Printf.sprintf "[%s,%s] %s"
        *            (string_of_iset q0)
        *            (string_of_ipset r0)
        *            acc 
        *        )
        *        (snd q)
        *        "")
        * ; *)
         let q' = fire_tau_2 q tt2 in
         if final1 q && not (final2 q')
         then Some h
         else
           let r1,td1 = 
             TSet.fold
               (fun t (acc,td) ->
                 match fire_tau_1 q' t with
                 | None -> (acc,td)
                 | Some q' ->
                    if test_link acc (fst q') (snd q')
                    then (acc,td)
                    else (add_link acc (fst q') (snd q'),(t::h,q')::td))
               tt1 (r,todo)
           in
           let r2,td2 =
             List.fold_left
               (fun (acc,td) (t1,t2) ->
                 match fire_var q' t1 t2 with
                 | None -> (acc,td)
                 | Some q' ->
                    if test_link acc (fst q') (snd q')
                    then (acc,td)
                    else (add_link acc (fst q') (snd q'),(t1::h,q')::td))
               (r1,td1)
               ta
           in check r2 td2
       end
  in
  let p0 = ISet.singleton i1
  and q0 = CmpStateSet.singleton (ISet.singleton i2,IPSet.singleton(i1,i2))
  in
  check (add_link init_sim p0 q0) [([],(p0,q0))]

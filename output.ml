(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
open Expr
open Tools

let rec string_of_expr = function
  | Zero -> "0"
  | Un -> "1"
  | V a -> a
  | Union(e,f) ->
     Printf.sprintf "%s + %s" (string_of_expr e) (string_of_expr f)
  | Seq((Union (_,_) as e),(Union (_,_) as f)) 
  | Seq((Union (_,_) as e),(Par (_,_) as f)) 
  | Seq((Par (_,_) as e),(Union (_,_) as f)) 
  | Seq((Par (_,_) as e),(Par (_,_) as f)) ->
     Printf.sprintf "(%s)(%s)" (string_of_expr e) (string_of_expr f)
  | Seq((Union (_,_) as e),f) 
  | Seq((Par (_,_) as e),f) ->
     Printf.sprintf "(%s)%s" (string_of_expr e) (string_of_expr f)
  | Seq(e,(Union (_,_) as f))
  | Seq(e,(Par (_,_) as f)) ->
     Printf.sprintf "%s(%s)" (string_of_expr e) (string_of_expr f)
  | Seq(e,f) ->
     Printf.sprintf "%s %s" (string_of_expr e) (string_of_expr f)
  | Par((Union (_,_) as e),(Union (_,_) as f)) ->
     Printf.sprintf "(%s)|(%s)" (string_of_expr e) (string_of_expr f)
  | Par((Union (_,_) as e),f) ->
     Printf.sprintf "(%s)| %s" (string_of_expr e) (string_of_expr f)
  | Par(e,(Union (_,_) as f)) ->
     Printf.sprintf "%s |(%s)" (string_of_expr e) (string_of_expr f)
  | Par(e,f) ->
     Printf.sprintf "%s | %s" (string_of_expr e) (string_of_expr f)
  | Star (V _ as e)
  | Star (Un as e)
  | Star (Zero as e) -> 
     Printf.sprintf "%s*"  (string_of_expr e)
  | Star e ->
     Printf.sprintf "(%s)*"  (string_of_expr e)

let string_of_comp = function
  | Geq -> ">="
  | Gt -> ">"
  | Leq -> "<="
  | Lt -> "<"
  | Incomp -> "<>"
  | Neq -> "=/="
  | Eq -> "="

let string_of_eq (c,e,f) = Printf.sprintf "%s %s %s"
					  (string_of_expr e)
					  (string_of_comp c)
					  (string_of_expr f)

open Graph
open Net
       
let graph_of_net (s,i,o,t : net) : io_hypergraph =
  let k0,i0,o0 =
    let k = ISet.max_elt s in
    (k+3,k+1,k+2)
  in
  let places = ISet.fold (fun i acc -> IMap.add i (string_of_int i) acc)
			 s IMap.empty in
  let links k s t acc =
    ISet.fold (fun p acc -> (p,k)::acc) s
	      (ISet.fold (fun p acc -> (k,p)::acc) t acc)
  in
  let trad k a =
    let t = function None -> "τ" | Some a -> a in
    Printf.sprintf "%d:%s" k (t a)
  in
  let trans,arcs =
    TSet.fold (fun (k,s,a,t) (tr,arcs) ->
	       (IMap.add (k+k0) (trad k a) tr,
		links (k+k0) s t arcs))
	      t (IMap.empty,[i0,i;o,o0])
  in
  (places,trans,arcs,ISet.of_list [i0;o0])

let dot_of_net n file = dot_of_graph (graph_of_net n) file

open Automaton

let graph_of_neauto (q,t,i,f : string neauto) : io_hypergraph =
  let st_to_str,str_to_st,k =
    List.fold_left
      (fun (acc1,acc2,k) q ->
       (IMap.add k q acc1,SMap.add q k acc2,k+1))
      (IMap.empty,SMap.empty,0)
      q
  in
  let lbl = function None -> "ε" | Some a -> a in
  let trans,arcs,k =
    List.fold_left
      (fun (tr,arcs,k) (p,a,q) ->
       IMap.add k (lbl a) tr,
       (SMap.find p str_to_st,k)::(k,SMap.find q str_to_st)::arcs,
       k+1)
      (IMap.empty,[],k) t
  in
  let points,arcs,_ =
    List.fold_left
      (fun (pts,arcs,k) q0 ->
       (ISet.add k pts,
	(k,SMap.find q0 str_to_st)::arcs,
	k+1))
      (List.fold_left
	 (fun (pts,arcs,k) qf ->
	  (ISet.add k pts,
	   (SMap.find qf str_to_st,k)::arcs,
	   k+1))
	 (ISet.empty,arcs,k)
	 f)
      i
  in
  (st_to_str,trans,arcs,points)

let dot_of_neauto a file =
  dot_of_graph (graph_of_neauto a) file
let dot_of_auto a file =
  dot_of_neauto (to_neauto a) file

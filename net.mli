(* Copyright (C) 2016 Paul Brunet
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program; if not, write to the Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*)
(** Labelled Petri Nets. *)


(** Module for the type of transitions in a Petri net. Such
    transitions are quadruples, composed of:
    - a unique identifier of integer type;
    - a set of inputs;
    - a label that is either empty or a [string]
    - a set of outputs.

 *)
type ptrans = int * Tools.ISet.t * string option * Tools.ISet.t

(** Sets of [ptrans]. *)
module TSet : Set.S with type elt = ptrans

(** Type of labelled Petri nets with inital and final places.  In
    [(p,i,o,t)], [p] is the set of places, [t] the set of transitions,
    and [i] and [o] are respectively the inital and final places. *)
type net = Tools.ISet.t * int * int * TSet.t

(** Builds the net associated with an expression. *)
val net_of_expr : Expr.t -> net

(** Builds the finite state automaton whose language is the set of
    accepting runs of a given net.*)
val auto_runs : net -> Tools.ISet.t Automaton.auto

(** Type of states in the product automaton: triples of a set of
    places of the first net, a set of places of the second net, and a
    binary relation between these two sets.*)
type sim_state = Tools.ISet.t * Tools.ISet.t * Tools.IPSet.t

(** Translation to string. *)
val string_of_sim_state : sim_state -> string
               
(** [prod_auto n1 n2] builds the finite state automaton obtained by
    trying to simulate [n2] using [n1]. Its alphabet is the set of
    transitions of [n2]. *)
val prod_auto : net -> net -> sim_state Automaton.auto

(** Prints a transition as a string of the shape [<i>:<a>], where [i]
    is the identifier of the transition, and [a] is either the label
    of the transition or {%html: &tau;%} if the label is empty. *)
val lbl : ptrans -> string

(** Converts back from string to transition, by parsing the string
    using the format above to get the identifier and then searching
    the given set of transitions to get one with the same id. *)
val trans_of_string : TSet.t -> string -> ptrans

(** Simply extracts the set of transition of a net. *)
val get_trans : net -> TSet.t

(** Test the inclusion of the language of a net into that of another. *)
val test_incl : net -> net -> ptrans list option                         
